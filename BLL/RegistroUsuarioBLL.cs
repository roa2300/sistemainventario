﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;
using DAL;

namespace BLL
{
    public class RegistroUsuarioBLL
    {
        private RegistroUsuarioDAL c = new RegistroUsuarioDAL();
        public void Add(RegistroUsuarios ob)
        { c.Add(ob); }

        public void Delete(int id)
        { c.Delete(id); }

        public void Update(RegistroUsuarios ob)
        { c.Update(ob); }

        public List<RegistroUsuarios> GetAll()
        { return c.GetAll(); }

        public RegistroUsuarios GetById(int id)
        { return c.GetById(id); }

        public void Save(RegistroUsuarios ob)
        { c.Save(ob); }
    }
}
