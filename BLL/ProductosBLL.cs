﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;
using DAL;

namespace BLL
{
    public class ProductosBLL
    {
        private ProductosDAL c = new ProductosDAL();

        public void Add(Productos ob)
        { c.Add(ob); }

        public void Delete(int id)
        { c.Delete(id); }

        public void Update(Productos ob)
        { c.Update(ob); }

        public List<Productos> GetAll()
        { return c.GetAll(); }

        public Productos GetById(int id)
        { return c.GetById(id); }

        public void Save(Productos ob)
        { c.Save(ob); }
    }
}
