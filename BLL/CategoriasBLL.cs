﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;
using DAL;

namespace BLL
{
    public class CategoriasBLL
    {
        private CategoriasDAL c = new CategoriasDAL();

        public void Add(Categorias ob)
        { c.Add(ob); }

        public void Delete(int id)
        { c.Delete(id); }

        public void Update(Categorias ob)
        { c.Update(ob); }

        public List<Categorias> GetAll()
        { return c.GetAll(); }

        public Categorias GetById(int id)
        { return c.GetById(id); }

        public void Save(Categorias ob)
        { c.Save(ob); }
    }
}
