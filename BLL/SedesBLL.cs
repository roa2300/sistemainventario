﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;
using DAL;

namespace BLL
{
    public class SedesBLL
    {
        private SedesDAL c = new SedesDAL();

        public void Add(Sedes ob)
        { c.Add(ob); }

        public void Delete(int id)
        { c.Delete(id); }

        public void Update(Sedes ob)
        { c.Update(ob); }

        public List<Sedes> GetAll()
        { return c.GetAll(); }

        public Sedes GetById(int id)
        { return c.GetById(id); }

        public void Save(Sedes ob)
        { c.Save(ob); }
    }
}
