﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;
using DAL;

namespace BLL
{
    public class TrabajadoresBLL
    {
        private TrabajadoresDAL c = new TrabajadoresDAL();

        public void Add(Trabajadores ob)
        { c.Add(ob); }

        public void Delete(int id)
        { c.Delete(id); }

        public void Update(Trabajadores ob)
        { c.Update(ob); }

        public List<Trabajadores> GetAll()
        { return c.GetAll(); }

        public Trabajadores GetById(int id)
        { return c.GetById(id); }

        public void Save(Trabajadores ob)
        { c.Save(ob); }
    }
}
