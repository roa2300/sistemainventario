﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using DAL.Encriptar;

using Entities;

namespace DAL
{
    public class RegistroUsuarioDAL: Conexion
    {
        string llavex = "1234";
        Encryptation e = new Encryptation();
        public List<RegistroUsuarios> ls = new List<RegistroUsuarios>();

        string sql = "";

        public RegistroUsuarioDAL() : base()
        {
        }

        public void Add(RegistroUsuarios ob)
        {
            sql = "insert into usuarios values ('{0}','{1}','{2}','{3}','{4}')";
            sql = string.Format(sql, ob.Id, e.Cifrar3DES(ob.Password, llavex), e.Cifrar3DES(ob.Nombre, llavex), e.Cifrar3DES(ob.Apellido,llavex), e.Cifrar3DES(ob.Correo,llavex));
            ExecuteNonQuery(sql);
        }

        public void Delete(int id)
        {
            sql = "delete from usuarios where id={0}";
            sql = string.Format(sql, id);
            ExecuteNonQuery(sql);
        }

        public void Update(RegistroUsuarios ob)
        {
            sql = "update usuarios set password='{1}',nombre='{2}',apellido='{3}',correo='{4}' where id='{0}'";
            sql = string.Format(sql, ob.Id, ob.Password, ob.Nombre, ob.Apellido, ob.Correo);
            ExecuteNonQuery(sql);
        }

        public List<RegistroUsuarios> GetAll()
        {
            List<RegistroUsuarios> ls = new List<RegistroUsuarios>();
            sql = "select * from usuarios";
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                RegistroUsuarios ob = new RegistroUsuarios();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Password = r[1].ToString();
                ob.Nombre = r[2].ToString();
                ob.Apellido = r[3].ToString();
                ob.Correo = r[4].ToString();
                ls.Add(ob);
            }
            return ls;
        }

        public RegistroUsuarios GetById(int id)
        {
            sql = "select * from usuarios where id={0}";
            sql = string.Format(sql, id);
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                RegistroUsuarios ob = new RegistroUsuarios();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Password = r[1].ToString();
                ob.Nombre = r[2].ToString();
                ob.Apellido = r[3].ToString();
                ob.Correo = r[4].ToString();
                return ob;
            }
            return null;
        }

        public void Save(RegistroUsuarios ob)
        {
            if (GetById(ob.Id) != null)
                Update(ob);
            else
                Add(ob);
        }
    }
}
