﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;

namespace DAL
{
    public class ProductosDAL : Conexion
    {
        string sql = "";

        public ProductosDAL() : base()
        {
        }

        public void Add(Productos ob)
        {
            sql = "insert into productos (id, nombre, id_categorias, preciocompra, precioventa, fechaingreso, id_trabajadores, id_sedes)";
            sql += "values ({0},'{1}',{2},'{3}','{4}','{5}',{6},{7})";
            sql = string.Format(sql, ob.Id, ob.Nombre, ob.Id_Categorias, ob.PrecioCompra, ob.PrecioVenta, ob.FechaIngreso.ToString("yyyy/MM/dd"),
                ob.Id_Trabajadores, ob.Id_Sedes);
            ExecuteNonQuery(sql);
        }

        public void Delete(int id)
        {
            sql = "delete from productos where id={0}";
            sql = string.Format(sql, id);
            ExecuteNonQuery(sql);
        }

        public void Update(Productos ob)
        {
            sql = "update productos set nombre='{1}', id_categorias={2}, preciocompra='{3}', precioventa='{4}', fechaingreso='{5}', id_trabajadores={6}, id_sedes={7} where id={0}";
            sql = string.Format(sql, ob.Id, ob.Nombre, ob.Id_Categorias, ob.PrecioCompra, ob.PrecioVenta, ob.FechaIngreso.ToString("yyyy/MM/dd"),
                ob.Id_Trabajadores, ob.Id_Sedes);
            ExecuteNonQuery(sql);
        }

        public List<Productos> GetAll()
        {
            List<Productos> ls = new List<Productos>();
            sql = "select * from productos";
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                Productos ob = new Productos();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Nombre = r[1].ToString();
                ob.Id_Categorias = Convert.ToInt32(r[2]);
                ob.PrecioCompra = Convert.ToDecimal(r[3]);
                ob.PrecioVenta = Convert.ToDecimal(r[4]);
                ob.FechaIngreso = Convert.ToDateTime(r[5]);
                ob.Id_Trabajadores = Convert.ToInt32(r[6]);
                ob.Id_Sedes = Convert.ToInt32(r[7]);
                ls.Add(ob);
            }
            return ls;
        }

        public Productos GetById(int id)
        {
            sql = "select * from productos where id={0}";
            sql = string.Format(sql, id);
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                Productos ob = new Productos();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Nombre = r[1].ToString();
                ob.Id_Categorias = Convert.ToInt32(r[2]);
                ob.PrecioCompra = Convert.ToDecimal(r[3]);
                ob.PrecioVenta = Convert.ToDecimal(r[4]);
                ob.FechaIngreso = Convert.ToDateTime(r[5]);
                ob.Id_Trabajadores = Convert.ToInt32(r[6]);
                ob.Id_Sedes = Convert.ToInt32(r[7]);
                return ob;
            }
            return null;
        }

        public void Save(Productos ob)
        {
            if (GetById(ob.Id) != null)
                Update(ob);
            else
                Add(ob);
        }
    }
}
