﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;

namespace DAL
{
    public class TrabajadoresDAL : Conexion
    {
        string sql = "";

        public TrabajadoresDAL() : base()
        {
        }

        public void Add(Trabajadores ob)
        {
            sql = "insert into trabajadores (id, nombre, apellido, direccion, telefono, genero, fechaingreso, foto, correo)";
            sql += "values ({0},'{1}','{2}','{3}',{4},'{5}','{6}','{7}','{8}')";
            sql = string.Format(sql, ob.Id, ob.Nombre, ob.Apellido, ob.Direccion, ob.Telefono, ob.Genero, ob.FechaIngreso.ToString("yyyy/MM/dd"), ob.Foto, ob.Correo);
            ExecuteNonQuery(sql);
        }

        public void Delete(int id)
        {
            sql = "delete from trabajadores where id={0}";
            sql = string.Format(sql, id);
            ExecuteNonQuery(sql);
        }

        public void Update(Trabajadores ob)
        {
            sql = "update trabajadores set nombre='{1}', apellido='{2}', direccion='{3}', telefono={4},  genero='{5}', fechaingreso='{6}', foto='{7}', correo='{8}' where id={0}";
            sql = string.Format(sql, ob.Id, ob.Nombre, ob.Apellido, ob.Direccion, ob.Telefono, ob.Genero, ob.FechaIngreso.ToString("yyyy/MM/dd"),
                    ob.Foto, ob.Correo);
            ExecuteNonQuery(sql);
        }

        public List<Trabajadores> GetAll()
        {
            List<Trabajadores> ls = new List<Trabajadores>();
            sql = "select * from trabajadores";
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                Trabajadores ob = new Trabajadores();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Nombre = r[1].ToString();
                ob.Apellido = r[2].ToString();
                ob.Direccion = r[3].ToString();
                ob.Telefono = Convert.ToInt32(r[4]);
                ob.Genero = r[5].ToString();
                ob.FechaIngreso = Convert.ToDateTime(r[6]);
                ob.Foto = r[7].ToString();
                ob.Correo = r[8].ToString();
                ls.Add(ob);
            }
            return ls;
        }

        public Trabajadores GetById(int id)
        {
            sql = "select * from trabajadores where id={0}";
            sql = string.Format(sql, id);
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                Trabajadores ob = new Trabajadores();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Nombre = r[1].ToString();
                ob.Apellido = r[2].ToString();
                ob.Direccion = r[3].ToString();
                ob.Telefono = Convert.ToInt32(r[4]);
                ob.Genero = r[5].ToString();
                ob.FechaIngreso = Convert.ToDateTime(r[6]);
                ob.Foto = r[7].ToString();
                ob.Correo = r[8].ToString();
                return ob;
            }
            return null;
        }

        public void Save(Trabajadores ob)
        {
            if (GetById(ob.Id) != null)
                Update(ob);
            else
                Add(ob);
        }
    }
}

