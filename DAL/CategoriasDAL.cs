﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace DAL
{
    public class CategoriasDAL : Conexion
    {
        string sql = "";

        public CategoriasDAL() : base()
        {
        }

        public void Add(Categorias ob)
        {
            sql = "insert into categorias (id, Nombre)";
            sql += "values ({0}, '{1}')";
            sql = string.Format(sql, ob.Id, ob.Nombre);
            ExecuteNonQuery(sql);
        }

        public void Delete(int id)
        {
            sql = "delete from categorias where id={0}";
            sql = string.Format(sql, id);
            ExecuteNonQuery(sql);
        }

        public void Update(Categorias ob)
        {
            sql = "update categorias set nombre='{1}' where id={0}";
            sql = string.Format(sql,ob.Id, ob.Nombre);
            ExecuteNonQuery(sql);
        }

        public List<Categorias> GetAll()
        {
            List<Categorias> ls = new List<Categorias>();
            sql = "select * from categorias";
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                Categorias ob = new Categorias();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Nombre = r[1].ToString();
                ls.Add(ob);
            }
            return ls;
        }

        public Categorias GetById(int id)
        {
            sql = "select * from categorias where id={0}";
            sql = string.Format(sql, id);
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                Categorias ob = new Categorias();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Nombre = r[1].ToString();
                return ob;
            }
            return null;
        }

        public void Save(Categorias ob)
        {
            if (GetById(ob.Id) != null)
                Update(ob);
            else
                Add(ob);
        }
    }
}
