﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace DAL
{
    public class SedesDAL : Conexion
    {
        string sql = "";

        public SedesDAL() : base()
        {
        }

        public void Add(Sedes ob)
        {
            sql = "insert into sedes (id, nombre, direccion, telefono, responsablesede)";
            sql += "values ({0},'{1}','{2}',{3},'{4}')";
            sql = string.Format(sql, ob.Id, ob.Nombre, ob.Direccion, ob.Telefono, ob.ResponsableSede);
            ExecuteNonQuery(sql);
        }

        public void Delete(int id)
        {
            sql = "delete from sedes where id={0}";
            sql = string.Format(sql, id);
            ExecuteNonQuery(sql);
        }

        public void Update(Sedes ob)
        {
            sql = "update sedes set nombre='{1}', direccion='{2}', telefono={3},  responsablesede='{4}' where id={0}";
            sql = string.Format(sql, ob.Id, ob.Nombre, ob.Direccion, ob.Telefono,  ob.ResponsableSede);
            ExecuteNonQuery(sql);
        }

        public List<Sedes> GetAll()
        {
            List<Sedes> ls = new List<Sedes>();
            sql = "select * from sedes";
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                Sedes ob = new Sedes();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Nombre = r[1].ToString();
                ob.Direccion = r[2].ToString();
                ob.Telefono = Convert.ToInt32(r[3]);
                ob.ResponsableSede = r[4].ToString();
                ls.Add(ob);
            }
            return ls;
        }

        public Sedes GetById(int id)
        {
            sql = "select * from sedes where id={0}";
            sql = string.Format(sql, id);
            DataTable dt = new DataTable();
            dt = ExecuteQuery(sql);
            foreach (DataRow r in dt.Rows)
            {
                Sedes ob = new Sedes();
                ob.Id = Convert.ToInt32(r[0]);
                ob.Nombre = r[1].ToString();
                ob.Direccion = r[2].ToString();
                ob.Telefono = Convert.ToInt32(r[3]);
                ob.ResponsableSede = r[4].ToString();
                return ob;
            }
            return null;
        }

        public void Save(Sedes ob)
        {
            if (GetById(ob.Id) != null)
                Update(ob);
            else
                Add(ob);
        }
    }
}
