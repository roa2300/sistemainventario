﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Entities;
using BLL;


namespace Presentation
{
    public partial class FrmSedes : Form
    {
        private SedesBLL t;

        private void FillGrid()
        {
            try
            {
                Dg1.DataSource = t.GetAll();
                Dg1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        [DllImport("user32.Dll", EntryPoint = "ReleaseCapture")]
        private extern static void RealeseCapture();
        [DllImport("user32.Dll", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int param);

        private void ClearText()
        {
            txtId.Text = "";
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
            txtRespoSede.Text = "";
        }

        private void FillData(int x)
        {
            try
            {
                var ob = t.GetById(x);
                if (ob != null)
                {
                    txtId.Text = ob.Id.ToString();
                    txtNombre.Text = ob.Nombre;
                    txtDireccion.Text = ob.Direccion;
                    txtTelefono.Text = ob.Telefono.ToString();
                    txtRespoSede.Text = ob.ResponsableSede;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Save()
        {
            try
            {
                Sedes ob = new Sedes();
                if (txtId.Text.Trim() != "")
                    ob.Id = Convert.ToInt32(txtId.Text);

                ob.Nombre = txtNombre.Text;
                ob.Direccion = txtDireccion.Text;
                ob.Telefono = Convert.ToInt32(txtTelefono.Text);
                ob.ResponsableSede = txtRespoSede.Text;

                t.Save(ob);
                FillGrid();
                ClearText();
                MessageBox.Show("Los datos han sido almacenados correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Update()
        {
            try
            {
                Sedes ob = new Sedes();
                if (txtId.Text.Trim() != "")
                    ob.Id = Convert.ToInt32(txtId.Text);

                ob.Nombre = txtNombre.Text;
                ob.Direccion = txtDireccion.Text;
                ob.Telefono = Convert.ToInt32(txtTelefono.Text);
                ob.ResponsableSede = txtRespoSede.Text;

                t.Update(ob);
                FillGrid();
                ClearText();
                MessageBox.Show("Los datos han sido actualizados correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Delete()
        {
            try
            {
                if (txtId.Text.Trim() != "")
                {
                    t.Delete(Convert.ToInt32(txtId.Text));
                    FillGrid();
                    ClearText();
                    MessageBox.Show("El registro ha sido eliminado correctamente");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

/****************************PATRON***********************/
        private static FrmSedes frm = null;
        private FrmSedes()
        {
            InitializeComponent();
        }

        public static FrmSedes GetInstance()
        {
            if (frm == null)
            {
                frm = new FrmSedes();
            }
            return frm;
        }
/****************************FINPATRON***********************/

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            Update();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void FrmSedes_Load(object sender, EventArgs e)
        {
            try
            {
                t = new SedesBLL();
                FillGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Dg1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                FillData(Convert.ToInt32(Dg1[0, e.RowIndex].Value));
            }
        }

        private void Dg1_CellContentDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                FillData(Convert.ToInt32(Dg1[0, e.RowIndex].Value));
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            if (txtId.Text.Trim() != "")
            {
                FillData(Convert.ToInt32(txtId.Text.Trim()));
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            RealeseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}
