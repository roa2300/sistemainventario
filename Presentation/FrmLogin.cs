﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Windows.Forms;
using Presentation.Micelania;
using System.Runtime.InteropServices;

namespace Presentation
{
    public partial class FrmLogin : Form
    {
        MySqlConnection conexion = new MySqlConnection("server=localhost;uid=root;pwd='1234';database=panama;port=3306");
        Encriptar uEncriptar = new Encriptar();
        string key = "1234";
        private int i = 0;
        //------------------------------------------------------
        private static FrmLogin instancia;

        public static FrmLogin getInstanse()
        {
            if (instancia == null || instancia.IsDisposed)
            {
                instancia = new FrmLogin();

            }
            instancia.BringToFront();
            return instancia;

        }

        [DllImport("user32.Dll", EntryPoint = "ReleaseCapture")]
        private extern static void RealeseCapture();
        [DllImport("user32.Dll", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int param);
        public FrmLogin()
        {
            InitializeComponent();
        }

        public bool validar()
        {
            if (txtUsuario.Text == "" || txtContra.Text == "")
            { return false; }
            else
            { return true; }
        }

        private void BtnIngresar_Click(object sender, EventArgs e)
        {
            if (validar() == true)
            {

                conexion.Close();
                conexion.Open();

                MySqlCommand cmd = new MySqlCommand("Select id, password from usuarios where id= '" + txtUsuario.Text + "' and password='" + uEncriptar.Cifrar3DES(txtContra.Text, key) + "'", conexion);
                MySqlDataReader read = cmd.ExecuteReader();

                if (read.Read())
                {
                    //MessageBox.Show("Conectado " + i);
                    Form1 lp = Form1.GetInstance();
                    lp.Show();
                    this.Hide();

                    //frm1.MdiParent = this;
                    //FrmPrincipal frm1 = FrmPrincipal.getInstanse();
                    //frm1.Show();

                    conexion.Close();
                }
                else if (i == 0)
                {
                    i++;
                    MessageBox.Show("Contraseña Incorrecta " + "intentos Restantes 2");
                    conexion.Close();

                }
                else if (i == 1)
                {


                    i++;
                    MessageBox.Show("Contraseña Incorrecta" + "intentos Restantes 1");
                    conexion.Close();

                }
                else if (i == 2)
                {


                    i++;
                    MessageBox.Show("Contraseña Incorrecta" +
                        "EL PROGRAMA SE CERRAR");
                    conexion.Close();
                    this.Dispose();
                }

            }
        }

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "ID")
            {
                txtUsuario.Text = "";
                txtUsuario.ForeColor = Color.LightGray;
            }
        }

        private void txtContra_Enter(object sender, EventArgs e)
        {
            if (txtContra.Text == "Password")
            {
                txtContra.Text = "";
                txtContra.ForeColor = Color.LightGray;
                txtContra.UseSystemPasswordChar = true;
            }
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "")
            {
                txtUsuario.Text = "ID";
                txtUsuario.ForeColor = Color.LightGray;
            }
        }

        private void txtContra_Leave(object sender, EventArgs e)
        {
            if (txtContra.Text == "")
            {
                txtContra.Text = "Password";
                txtContra.ForeColor = Color.LightGray;
                txtContra.UseSystemPasswordChar = false;
            }
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void contenedor_MouseDown(object sender, MouseEventArgs e)
        {
            RealeseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panellogo_MouseDown(object sender, MouseEventArgs e)
        {
            RealeseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}
