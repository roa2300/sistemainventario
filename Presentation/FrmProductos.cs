﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Entities;
using BLL;
using System.Runtime.InteropServices;

namespace Presentation
{
    public partial class FrmProductos : Form
    {
        private ProductosBLL pbll;
        private CategoriasBLL cbll;
        private TrabajadoresBLL tbll;
        private SedesBLL sbll;

        private void FillCombos()
        {
            cbxIdCategorias.DataSource = cbll.GetAll();
            cbxIdCategorias.DisplayMember = "Nombre";
            cbxIdCategorias.ValueMember = "Id";
            cbxIdCategorias.Refresh();

            cbxIdTrabajadores.DataSource = tbll.GetAll();
            cbxIdTrabajadores.DisplayMember = "Nombre";
            cbxIdTrabajadores.ValueMember = "Id";
            cbxIdTrabajadores.Refresh();

            cbxIdSedes.DataSource = sbll.GetAll();
            cbxIdSedes.DisplayMember = "Nombre";
            cbxIdSedes.ValueMember = "Id";
            cbxIdSedes.Refresh();
        }

        [DllImport("user32.Dll", EntryPoint = "ReleaseCapture")]
        private extern static void RealeseCapture();
        [DllImport("user32.Dll", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int param);

        private void FillGrid()
        {
            try
            {
                Dg1.DataSource = pbll.GetAll();
                Dg1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearText()
        {
            txtId.Text = "";
            txtNombre.Text = "";
            cbxIdCategorias.Text = "";
            txtPrecioCompra.Text = "";
            txtPrecioVenta.Text = "";
            dateTimeFecha.Text = "";
            cbxIdTrabajadores.Text = "";
            cbxIdSedes.Text = "";

        }

        private void FillData(int id)
        {
            try
            {
                pbll = new ProductosBLL();
                Productos ob = pbll.GetById(id);
                if (ob != null)
                {
                    txtId.Text = ob.Id.ToString();
                    txtNombre.Text = ob.Nombre;
                    cbxIdCategorias.Text = ob.Id_Categorias.ToString();
                    txtPrecioCompra.Text = ob.PrecioCompra.ToString();
                    txtPrecioVenta.Text = ob.PrecioVenta.ToString();
                    dateTimeFecha.Value = ob.FechaIngreso;
                    cbxIdTrabajadores.Text = ob.Id_Trabajadores.ToString();
                    cbxIdSedes.Text = ob.Id_Sedes.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Save()
        {
            try
            {
                Productos ob = new Productos();
                if (txtId.Text.Trim() != "")
                    ob.Id = Convert.ToInt32(txtId.Text);

                ob.Nombre = txtNombre.Text;
                ob.Id_Categorias = Convert.ToInt32(cbxIdCategorias.SelectedValue.ToString());
                ob.PrecioCompra = Convert.ToDecimal(txtPrecioCompra.Text);
                ob.PrecioVenta = Convert.ToDecimal(txtPrecioVenta.Text);
                ob.FechaIngreso = dateTimeFecha.Value;
                ob.Id_Trabajadores = Convert.ToInt32(cbxIdTrabajadores.SelectedValue.ToString());
                ob.Id_Sedes = Convert.ToInt32(cbxIdSedes.SelectedValue.ToString());

                pbll.Save(ob);
                FillGrid();
                ClearText();
                MessageBox.Show("Los datos han sido almacenados correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Update()
        {
            try
            {
                Productos ob = new Productos();
                if (txtId.Text.Trim() != "")
                    ob.Id = Convert.ToInt32(txtId.Text);

                ob.Nombre = txtNombre.Text;
                ob.Id_Categorias = Convert.ToInt32(cbxIdCategorias.SelectedValue);
                ob.PrecioCompra = Convert.ToDecimal(txtPrecioCompra.Text);
                ob.PrecioVenta = Convert.ToDecimal(txtPrecioVenta.Text);
                ob.FechaIngreso = dateTimeFecha.Value;
                ob.Id_Trabajadores = Convert.ToInt32(cbxIdTrabajadores.SelectedValue);
                ob.Id_Sedes = Convert.ToInt32(cbxIdSedes.SelectedValue);

                pbll.Update(ob);
                FillGrid();
                ClearText();
                MessageBox.Show("Los datos han sido actualizados correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Delete()
        {
            try
            {
                if (txtId.Text.Trim() != "")
                {
                    pbll.Delete(Convert.ToInt32(txtId.Text));
                    FillGrid();
                    ClearText();
                    MessageBox.Show("El registro ha sido eliminado correctamente");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
 /****************************PATRON***********************/
        private static FrmProductos frm = null;
        private FrmProductos()
        {
            InitializeComponent();
        }

        public static FrmProductos GetInstance()
        {
            if (frm == null)
            {
                frm = new FrmProductos();
            }
            return frm;
        }
/****************************FINPATRON***********************/

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            Update();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            FillData(Convert.ToInt32(txtId.Text));
        }

        private void FrmProductos_Load(object sender, EventArgs e)
        {
            try
            {
                pbll = new ProductosBLL();
                cbll = new CategoriasBLL();
                tbll = new TrabajadoresBLL();
                sbll = new SedesBLL();
                FillGrid();
                FillCombos();
                ClearText();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Dg1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                FillData(Convert.ToInt32(Dg1[0, e.RowIndex].Value));
            }
        }

        private void Dg1_CellContentDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                FillData(Convert.ToInt32(Dg1[0, e.RowIndex].Value));
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            RealeseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}
