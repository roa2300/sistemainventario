﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Entities;
using BLL;
using DAL;

namespace Presentation
{
    public partial class FrmRegistroUsuario : Form
    {
        private RegistroUsuarioBLL t;

        private void FillGrid()
        {
            try
            {
                Dg1.DataSource = t.GetAll();
                Dg1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ClearText()
        {
            txtId.Clear();
            txtPassword.Text = "";
            txtCorreo.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtId.Focus();
            Dg1.Text = "";
        }

        public void Save()
        {
            try
            {
                RegistroUsuarios ob = new RegistroUsuarios();
                ob.Id = Convert.ToInt32(txtId.Text);
                ob.Password = txtPassword.Text;
                ob.Nombre = txtNombre.Text;
                ob.Apellido = txtApellido.Text;
                ob.Correo = txtCorreo.Text;
                


                t.Save(ob);
                FillGrid();
                ClearText();
                MessageBox.Show("Los datos han sido almacenados correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Update()
        {
            try
            {
                RegistroUsuarios ob = new RegistroUsuarios();
                ob.Id = Convert.ToInt32(txtId.Text);
                ob.Password = txtPassword.Text;
                ob.Nombre = txtNombre.Text;
                ob.Apellido = txtApellido.Text;
                ob.Correo = txtCorreo.Text;



                t.Update(ob);
                FillGrid();
                ClearText();
                MessageBox.Show("Los datos han sido actualizacion correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void Delete()
        {
            try
            {
                if (txtId.Text.Trim() != "")
                {
                    t.Delete(Convert.ToInt32(txtId.Text));
                    FillGrid();
                    ClearText();
                    MessageBox.Show("El registro ha sido eliminado correctamente");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillData(int x)
        {
            try
            {
                var ob = t.GetById(x);
                if (ob != null)
                {
                    txtId.Text = ob.Id.ToString();
                    txtPassword.Text = ob.Password;
                    txtNombre.Text = ob.Nombre;
                    txtApellido.Text = ob.Apellido;
                    txtCorreo.Text = ob.Correo;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public FrmRegistroUsuario()
        {
            InitializeComponent();
        }

        private void FrmRegistroUsuario_Load(object sender, EventArgs e)
        {
            try
            {
                t = new RegistroUsuarioBLL();
                FillGrid();
                ClearText();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnRegistrar_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            Update();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void Dg1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                FillData(Convert.ToInt32(Dg1[0, e.RowIndex].Value));
            }
        }

        private void Dg1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                FillData(Convert.ToInt32(Dg1[0, e.RowIndex].Value));
            }
        }
    }
}
