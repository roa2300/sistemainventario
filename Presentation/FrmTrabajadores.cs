﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Entities;
using BLL;
using System.Runtime.InteropServices;

namespace Presentation
{
    public partial class FrmTrabajadores : Form
    {
        private TrabajadoresBLL t;

        private void FillGrid()
        {
            try
            {
                Dg1.DataSource = t.GetAll();
                Dg1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        [DllImport("user32.Dll", EntryPoint = "ReleaseCapture")]
        private extern static void RealeseCapture();
        [DllImport("user32.Dll", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int param);

        private void ClearText()
        {
            txtId.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
            rbHombre.Checked = true;
            rbMujer.Checked = false;
            dtfechaingreso.Text = "";
            txtFoto.Text = "";
            txtCorreo.Text = "";

            picImagen.Image = null;
        }

        private void FillData(int id)
        {
            try
            {
                t = new TrabajadoresBLL();
                Trabajadores ob = t.GetById(id);
                if (ob != null)
                {
                    txtId.Text = ob.Id.ToString();
                    txtNombre.Text = ob.Nombre;
                    txtApellido.Text = ob.Apellido;
                    txtDireccion.Text = ob.Direccion;
                    txtTelefono.Text = ob.Telefono.ToString();
                    dtfechaingreso.Value = ob.FechaIngreso;
                    ob.Foto = txtFoto.Text.Replace("\\", "//");
                    txtCorreo.Text = ob.Correo;

                    if (ob.Genero == "Masculino")
                        rbHombre.Checked = true;

                    else
                        rbMujer.Checked = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Save()
        {
            try
            {
                Trabajadores ob = new Trabajadores();
                if (txtId.Text.Trim() != "")
                    ob.Id = Convert.ToInt32(txtId.Text);

                ob.Nombre = txtNombre.Text;
                ob.Apellido = txtApellido.Text;
                ob.Direccion = txtDireccion.Text;
                ob.Telefono = Convert.ToInt32(txtTelefono.Text);
                ob.FechaIngreso = dtfechaingreso.Value;
                ob.Foto = txtFoto.Text.Replace("\\", "//");
                ob.Correo = txtCorreo.Text;

                if (rbHombre.Checked)
                    ob.Genero = "Masculino";

                else
                    ob.Genero = "Femenino";

                t.Save(ob);
                FillGrid();
                ClearText();
                MessageBox.Show("Los datos han sido almacenados correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Update()
        {
            try
            {
                Trabajadores ob = new Trabajadores();
                if (txtId.Text.Trim() != "")
                    ob.Id = Convert.ToInt32(txtId.Text);

                ob.Nombre = txtNombre.Text;
                ob.Apellido = txtApellido.Text;
                ob.Direccion = txtDireccion.Text;
                ob.Telefono = Convert.ToInt32(txtTelefono.Text);
                ob.FechaIngreso = dtfechaingreso.Value;
                ob.Foto = txtFoto.Text.Replace("\\", "//");
                ob.Correo = txtCorreo.Text;

                if (rbHombre.Checked)
                    ob.Genero = "Masculino";

                else
                    ob.Genero = "Femenino";

                t.Update(ob);
                FillGrid();
                ClearText();
                MessageBox.Show("Los datos han sido actualizados correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Delete()
        {
            try
            {
                if (txtId.Text.Trim() != "")
                {
                    t.Delete(Convert.ToInt32(txtId.Text));
                    FillGrid();
                    ClearText();
                    MessageBox.Show("El registro ha sido eliminado correctamente");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
/****************************PATRON***********************/
        private static FrmTrabajadores frm = null;
        private FrmTrabajadores()
        {
            InitializeComponent();
        }

        public static FrmTrabajadores GetInstance()
        {
            if (frm == null)
            {
                frm = new FrmTrabajadores();
            }
            return frm;
        }
/****************************FINPATRON***********************/

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            Update();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void FrmTrabajadores_Load(object sender, EventArgs e)
        {
            try
            {
                t = new TrabajadoresBLL();
                FillGrid();
                ClearText();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Dg1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                FillData(Convert.ToInt32(Dg1[0, e.RowIndex].Value));
            }
        }


        private void Dg1_CellContentDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                FillData(Convert.ToInt32(Dg1[0, e.RowIndex].Value));
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            FillData(Convert.ToInt32(txtId.Text));
        }

        private void picImagen_Click(object sender, EventArgs e)
        {
            {
                if (openFile.ShowDialog() != DialogResult.Cancel)
                {

                    txtFoto.Text = openFile.FileName;
                    picImagen.Image = Image.FromFile(txtFoto.Text);

                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            RealeseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}
