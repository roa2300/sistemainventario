﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation
{
    public partial class Form1 : Form
    {

 /****************************PATRON***********************/
        private static Form1 frm = null;
        private Form1()
        {
            InitializeComponent();
        }

        public static Form1 GetInstance()
        {
            if (frm == null)
            {
                frm = new Form1();
            }
            return frm;
        }
/****************************FINPATRON***********************/

        [DllImport("user32.Dll", EntryPoint = "ReleaseCapture")]
        private extern static void RealeseCapture();
        [DllImport("user32.Dll", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int param);



        private void BtnSlider_Click(object sender, EventArgs e)
        {
        }

        private void IconCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
        private void IconMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            RealeseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void HoraFecha_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToLongTimeString();
            lblMeses.Text = DateTime.Now.ToShortDateString();
        }

        private void BtnCategorias_Click(object sender, EventArgs e)
        {
            FrmCategorias categoria1=  FrmCategorias.GetInstance();
            categoria1.MdiParent = this;
            categoria1.Show();
            categoria1.BringToFront();

        }

        private void BtnProducto_Click(object sender, EventArgs e)
        {
            FrmProductos productos1 = FrmProductos.GetInstance();
            productos1.MdiParent = this;
            productos1.Show();
            productos1.BringToFront();
        }

        private void BtnSedes_Click(object sender, EventArgs e)
        {
            FrmSedes sedes1 = FrmSedes.GetInstance();
            sedes1.MdiParent = this;
            sedes1.Show();
            sedes1.BringToFront();
        }

        private void BtnTrabajadores_Click(object sender, EventArgs e)
        {
            FrmTrabajadores trabajadores1 = FrmTrabajadores.GetInstance();
            trabajadores1.MdiParent = this;
            trabajadores1.Show();
            trabajadores1.BringToFront();
        }
    }
}
