﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Micelania
{
    public class Encriptar
    {
        public string Cifrar3DES(string cadena, string llave)
        {
            string cad = "";
            byte[] keyArray;
            byte[] Arreglo_a_cifrar = UTF8Encoding.UTF8.GetBytes(cadena);

            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(llave));

            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();

            byte[] ArrayResultado = cTransform.TransformFinalBlock(Arreglo_a_cifrar, 0, Arreglo_a_cifrar.Length);
            tdes.Clear();
            cad = Convert.ToBase64String(ArrayResultado, 0, ArrayResultado.Length);
            return cad;
        }

        public string Decifrar3DES(string cadena, string llave)
        {
            try
            {
                string cad = "";
                byte[] keyArray;
                byte[] Array_a_Descifrar = Convert.FromBase64String(cadena);

                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(llave));

                hashmd5.Clear();
                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateDecryptor();

                byte[] resultArray = cTransform.TransformFinalBlock(Array_a_Descifrar, 0, Array_a_Descifrar.Length);

                tdes.Clear();
                cad = UTF8Encoding.UTF8.GetString(resultArray);
                return cad;
            }
            catch
            {
                return "";
            }
        }
    }
}
