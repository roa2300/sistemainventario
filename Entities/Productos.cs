﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Productos
    {
        public int Id { get; set; }

        public string Nombre { get; set; }

        public int Id_Categorias { get; set; }

        public decimal PrecioCompra { get; set; }

        public decimal PrecioVenta { get; set; }

        public DateTime FechaIngreso { get; set; }

        public int Id_Trabajadores { get; set; }

        public int Id_Sedes { get; set; }

    }
}
